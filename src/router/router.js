import Vue from "vue";
import VueRouter from "vue-router";
const Admin = ()=>import('../components/feature/admin/Admin.vue')
const Login = ()=>import('../components/feature/auth/Login.vue')
const StatisticShop = ()=> import('../components/feature/statistics/StatisticShop')
const StatisticBrandFlow = ()=> import('../components/feature/statistics/StatisticBrandFlow')
const StatisticCreated = ()=> import('../components/feature/statistics/StatisticCreated')
const PositionCss = ()=> import('../components/feature/learCss/PositionCss.vue')
const Animations= ()=> import('../components/feature/learCss/Animations.vue')
Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Login",
    component: Login,
  }, 
  {
    path: "/admin",
    name: "Admin",
    component: Admin,
    meta: { layout: "admin" },
    children: [
      {  
        // UserProfile will be rendered inside User's <router-view>
        // when /user/:id/profile is matched
        path: 'statisticShop',
        name:'statisticShop',
        component: StatisticShop,
        meta: { layout: "admin" },
      },
      {
        // UserPosts will be rendered inside User's <router-view>
        // when /user/:id/posts is matched
        path: 'statisticBrandFlow',
        name:'statisticBrandflow',
        component: StatisticBrandFlow,
        meta: { layout: "admin" },
      },
      {
        path:'statisticCreated',
        name:'statisticCreated',
        component:StatisticCreated,
        meta: { layout: "admin" },
      },
      {
        path:'positionCss',
        name:'positionCss',
        component:PositionCss,
        meta: { layout: "admin" },
      },
      {
        path:'animations',
        name:'animations',
        component:Animations,
        meta: { layout: "admin" },
      }
    ],
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;

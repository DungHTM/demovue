import vuetify from '@/plugins/vuetify'
import Vue from 'vue'
import App from './App.vue'
import i18n from './i18n'
import AuthLogin from './components/share/AuthLogin'
import Home from './components/share/Home'
import router from "./router/router.js";
import FlagIcon from "vue-flag-icon";
import store from './store/store'
import './directive/directive'
Vue.config.productionTip = false
Vue.component('auth-login',AuthLogin)
Vue.component('admin',Home) 
Vue.use(FlagIcon) 
new Vue({
  router,
  i18n,
  store,
  vuetify,
  render: h => h(App),
}).$mount('#app')

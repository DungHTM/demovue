import Vue from 'vue' 
Vue.directive('scroll',{
    bind(el, binding, vnode){
        console.log({el,binding, vnode})
        el.style.position = "fixed";
        el.style.top = `${binding.value.top}px`;
        el.style.left = `${binding.value.left}px`
    }
  }) 

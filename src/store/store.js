import Vuex from "vuex";
import Vue from "vue";
import { shop, auth } from "./modules/index";
Vue.use(Vuex);
const heightTop =  localStorage.getItem("heightTop")|| 0
const store = new Vuex.Store({
  modules: {
    auth: auth,
    shop: shop,
  },
  state: {
    lang: process.env.VUE_APP_I18N_LOCALE || "us",
    heightHeader:heightTop
  },
  mutations: {
    lang: (state, { lang }) => {
      state.lang = lang;
    },
    heightHeader:(state,heightHeader)=>{
        state.heightHeader = heightHeader
        localStorage.setItem("heightTop",heightHeader)
    }
  },
  getters: {
    lang: (state) => {
      return state.lang;
    },
    heightHeader: (state) => {
        return state.heightHeader;
      },
  },
});

export default store;

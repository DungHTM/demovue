module.exports = {
    module: {
      rules: [
        {
          test: /\.s(c|a)ss$/,
          use: [
            'vue-style-loader',
            'css-loader',
            {
              loader: 'sass-loader',
              // Requires sass-loader@^7.0.0
              options: {
                implementation: require('sass'),
                indentedSyntax: true // optional
              },
              // Requires >= sass-loader@^8.0.0
              options: {
                implementation: require('sass'),
                sassOptions: {
                  indentedSyntax: true // optional
                },
              },
            },
          ],
        },
      ],
    }
  }

  //boss

//   // const path = require('path')
// const fs = require('fs')
// const webpack = require('webpack') 
// const packageJson = fs.readFileSync('./package.json')
// const version = JSON.parse(packageJson).version || 0

// module.exports = {
//     transpileDependencies: [
//       'vuetify'
//     ],
//     configureWebpack: {
//         plugins: [
//            new webpack.DefinePlugin({
//                'process.env': {
//                    PACKAGE_VERSION: '"' + version + '"'
//                }
//            })
//         ]
//     }
// }
